#ifndef _OPENGL_UTILS
#define _OPENGL_UTILS

// OpenGL includes
#include <GL/glew.h>
//#include <GL/freeglut.h>
#include <GLFW/glfw3.h>

// Standard includes
#include <iostream>
#include <string>
#include <vector>

using namespace std;

// Quelques outils tres utils pour le graphisme avec OpenGL

// Nous encapsulons nos Shader et leur type
struct ShaderSource {
	unsigned int type;
	string source;
};

// On peut compiler les shaders des shaders_sources avec cette fonction
// On obtien alors un tableau contenant les id de nos shaders compiles
unsigned int * compileShaders(vector<ShaderSource> shaders_sources);


namespace mOpengl {

	// On peut creer un programme contenant nos shaders compiles
	unsigned int createShadingProgram(vector<ShaderSource> shaders_sources);

	// On peut effacer toutes les erreurs contenu dans openGL
	void errorClearGLError();

	// On peut aussi les afficher
	bool errorCheckGLError();

	// On peut lancer OpenGL en plusieurs etapes avec cette fonction
	// * Initialiser GLFW
	// * Creer une fenetre GLFW
	// * La rendre context courant
	// * Initialiser GLEW 
	GLFWwindow * startOpenGL(unsigned int width, unsigned int heigth, const char * title);

	// On peut afficher la version de OpenGL
	void displayOpenGLVersion();
}

#endif