#ifndef _DRAWING_
#define _DRAWING_

// OpenGL includes
#include <GL/glew.h>
//#include <GL/freeglut.h>

#include <iostream>
#include <string>

using namespace std;

// Quelques Types et fonctions pour dessiner avec OpenGL

// Donnees a ecrire dans un buffer pour dessiner le carre correspondant
struct Square {
	// Format x, y, couleur
	float ax; float ay; float ca;
	float bx; float by; float cb;
	float cx; float cy; float cc;
	float dx; float dy; float cd;
};

/* Ces valeurs constantes sont en nombre de floatants*/

// La taille de la structure Square
const unsigned int SQUARE_DATA_SIZE = 12;

// L'attribut definissant les positions de chaque point (Vertex)
const unsigned int SQUARE_ATTRIB_INDEX_VERTEX_POSITION = 0;
const unsigned int SQUARE_START_OF_VERTECES = 0;
const unsigned int SQUARE_SPACE_BETWEEN_VERTEX = 0;
const unsigned int SQUARE_VERTEX_SIZE = 2;
const unsigned int SQUARE_VERTECES_OFFSET = 3;

// L'attribut definissant la couleur du carre
const unsigned int SQUARE_ATTRIB_INDEX_COLOR = 1;
const unsigned int SQUARE_START_OF_COLOR = 2;
const unsigned int SQUARE_COLOR_SIZE = 1;
const unsigned int SQUARE_COLOR_OFFSET = 3;

// Indices pour dessiner nos carres en utilisant deux triangles
const unsigned int SQUARE_DRAW_INDICES[] = {
	0, 1, 2,
	0, 2, 3
};

namespace Drawing {

	// On peut avoir les informations d'un Square
	string getStringOfSquare(Square sq);

	// On peut ecrire un Square dans un buffer dans le GPU
	// On obtient alors l'identifiant de ce buffer
	unsigned int setSquareBuffer(Square q);

	// On peut definir a tout moment les attributs que OpenGL enverra a nous Shader pour dessine un Square
	void setSquareAttributes();

	// On peut dessiner le Square actuellement dans le buffer du GPU
	// avec les attributs fixes
	// ainsi que le program de shader defini
	// (Prerequis : compiler un programme de shader, ajouter un Square au buffer, fixer les attributs de dessin de Square)
	void drawSquare();
}

#endif