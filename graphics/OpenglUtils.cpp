#include "OpenglUtils.h"

// FONCTIONS LOCALES AUXILIAIRES: Pour mener a bien nos operations dans Utiles, nous aurons besoin de : *******************

// Compiler un shader et avoir son id de shader compile
unsigned int compileShader(unsigned int type, const string & source) {
	// Pour compiler un shader, 

	// On creer un nouveau  shader dans openGL et on obtient son id
	unsigned int new_shader_id = glCreateShader(type);

	// On ajoute a notre nouveau shader dans OpenGL un code source
	const char * src = source.c_str();
	int nbr_sources_code = 1;
	glShaderSource(new_shader_id, nbr_sources_code, &src, nullptr);

	// On compile notre shader dans OpenGL
	glCompileShader(new_shader_id);

	// On verifi s'il n'y a pas d'erreurs
	int compile_result;
	glGetShaderiv(new_shader_id, GL_COMPILE_STATUS, &compile_result);
	if(compile_result == GL_FALSE) {
		// S'il y a des erreurs, on les affiche
		int error_message_length;
		glGetShaderiv(new_shader_id, GL_INFO_LOG_LENGTH, &error_message_length);
		char * error_message = new char[error_message_length];
		glGetShaderInfoLog(new_shader_id, error_message_length, &error_message_length, error_message);
		cout << "Fail compiling shader : " << error_message << endl;
		exit(1);
	}

	return new_shader_id;
}

// Attacher des shaders compiles a un program
// Pour ca, on a besoin des id de ces shaders (shaders_ids)
// Et de l'id du programme auquel on veut les attacher (new_program_id)
// (l'argument size est le nombre d'id de shaders)
void attachShaders(unsigned int new_program_id, unsigned int * shaders_ids, int size) {
	for (int i = 0; i < size; ++i) {
		glAttachShader(new_program_id, shaders_ids[i]);	
	}
}

// Supprimer de OpenGL les shaders compiles dont on a plus besoin
// Pour ca, on a besoin de leur id (shaders_ids)
// (size est le nombre d'id de shaders)
void deleteShadersLeftOver(unsigned int * shaders_ids, int size) {
	for (int i = 0; i < size; ++i) {
		glDeleteShader(shaders_ids[i]);
	}
}

// FONCTIONS PRINCIPALES **************************************************************

unsigned int * compileShaders(vector<ShaderSource> shaders_sources) {
	unsigned int * shaders_ids = new unsigned int[shaders_sources.size()];
	int i = 0;
	for (vector<ShaderSource>::iterator it = shaders_sources.begin(); it != shaders_sources.end() ; ++it) {
		ShaderSource sc = (*it);
		shaders_ids[i] = compileShader(sc.type, sc.source);
		i++;
	}
	return shaders_ids;
}

namespace mOpengl {

	unsigned int createShadingProgram(vector<ShaderSource> shaders_sources) {
		unsigned int new_program_id = glCreateProgram();

		unsigned int * shaders_ids = compileShaders(shaders_sources);
		attachShaders(new_program_id, shaders_ids, shaders_sources.size());

		glLinkProgram(new_program_id);
		glValidateProgram(new_program_id);

		deleteShadersLeftOver(shaders_ids, shaders_sources.size());

		return new_program_id;
	}

	void errorClearGLError() {
		while(glGetError() != GL_NO_ERROR);
	}

	bool errorCheckGLError() {
		unsigned int error_count = 0;
		while(GLenum error = glGetError()) {
			cout << "OpenGL Error : " << error << endl;
			error_count ++;
		}
		if(error_count > 0)
			return true;
		return false;
	}

	GLFWwindow * startOpenGL(unsigned int width, unsigned int heigth, const char * title) {
		// Pour lancer OpenGL, 
		GLFWwindow* window;

		// On initialise de GLFW
	    if (!glfwInit()) {
	    	cout << "Fail to init GLFW" << endl;
	        exit(1);
	    }
	    // On cree une fenetre glfw pour qui vas accuiellir nos graphismes openGL
	    window = glfwCreateWindow(width, heigth, title, NULL, NULL);
	    if (!window) {
	    	cout << "Fail to create window" << endl;
	        glfwTerminate();
	        exit(1);
	    }

	    // On defini notre nouvelle fenetre comme la fenetre courante
	    glfwMakeContextCurrent(window);

	    // On initialise de GLEW
	    if(glewInit() != GLEW_OK) {
	    	cout << "Fail to init GLEW" << endl;
	    	exit(1);
	    }

	    // Et on retourne la nouvelle fenetre
	    return window;
	}
	
	void displayOpenGLVersion() {
		const unsigned char * s = glGetString(GL_SHADING_LANGUAGE_VERSION);
		cout << "Shading language version (GLSL) : " << s << endl;
		s = glGetString(GL_VERSION);
		cout << "OpenGL version : " << s << endl;
	}
}
