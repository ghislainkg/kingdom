#include "EntityDrawing.h"

namespace EntityDrawing {
	void drawEntities(vector<Entity*> * entities) {
        for (vector<Entity*>::iterator it = entities->begin(); it != entities->end(); ++it) {
            Square sq = (*it)->getSquare();

            errorClearGLError();
            setSquareBuffer(sq);
            errorCheckGLError();

            errorClearGLError();
            setSquareAttributes();
            errorCheckGLError();

            errorClearGLError();
            drawSquare();
            errorCheckGLError();
        }
    }
}