#ifndef _ENTITY_DRAWING_
#define _ENTITY_DRAWING_

#include <vector>

#include "../Initialisation/Entity.h"
#include "../DataClass/Person.h"
#include "Drawing.h"
#include "OpenglUtils.h"

using namespace std;
using namespace Drawing;
using namespace mOpengl;

// Quelques fonctions pour dessiner avec des Entity OpenGL

namespace EntityDrawing {
	void drawEntities(vector<Entity*> * entities);
}

#endif