#include "Drawing.h"

void setFloatAttribut(
    GLuint attrib_index,
    GLint attrib_size,
    GLsizei offset_between_attrib,
    const void * p_first_attrib) {

    glEnableVertexAttribArray(attrib_index);
    glVertexAttribPointer(
        attrib_index,
        attrib_size,
        GL_FLOAT,
        GL_FALSE, 
        offset_between_attrib,
        p_first_attrib 
    );
}

namespace Drawing {
    string getStringOfSquare(Square sq) {
        float * aux = (float*)&sq;
        string result = "";
        for (int i = 0; i < 12; ++i) {
            result += " " + to_string(aux[i]) ;
            if(i%3 == 2)
                result += "\n";
        }
        return result;
    }
    // Retourne l'id du buffer
    unsigned int setSquareBuffer(Square q) {
    	unsigned int buffer_id;

    	// Creer notre buffer dans le GPU
    	glGenBuffers(1, &buffer_id);
        
    	// Bind notre buffer a l'etat courant
        glBindBuffer(GL_ARRAY_BUFFER, buffer_id);

        // Met les donnees de notre carre dans le buffer courant
        glBufferData(GL_ARRAY_BUFFER,
        	SQUARE_DATA_SIZE * sizeof(float),
        	&q,
        	GL_STATIC_DRAW
        );

        return buffer_id;
    }

    // Defini les attributs d'un square
    void setSquareAttributes() {
    	// Attribut definissant la position de chaque vertex
        setFloatAttribut(
        	SQUARE_ATTRIB_INDEX_VERTEX_POSITION,
        	SQUARE_VERTEX_SIZE,
        	SQUARE_VERTECES_OFFSET*sizeof(float),
            (void *) ( SQUARE_START_OF_VERTECES * sizeof(float))
        );
        /*Attribut definissant la couleur de chaque vertex*/
        setFloatAttribut(
        	SQUARE_ATTRIB_INDEX_COLOR,
        	SQUARE_COLOR_SIZE,
        	SQUARE_COLOR_OFFSET*sizeof(float),
            (void *) ( SQUARE_START_OF_COLOR * sizeof(float))
        );
    }

    void drawSquare() {
        unsigned int indice_buffer_id;
        glGenBuffers(1, &indice_buffer_id);
        glBindBuffer(GL_ELEMENT_ARRAY_BUFFER, indice_buffer_id);
        glBufferData(GL_ELEMENT_ARRAY_BUFFER,
            sizeof(SQUARE_DRAW_INDICES),
            SQUARE_DRAW_INDICES,
            GL_STATIC_DRAW
        );
        glDrawElements(
            GL_TRIANGLES, // On dessine des triangles
            6, // 6 indices
            GL_UNSIGNED_INT, // indices de type unsigned int
            0 // On commence au premier indice
            //Utilise tous les indices pour dessiner des triangles
            //Jusqu'a ce qu'il n'y ai plus d'indice (6 / 2 = 3, 2 triangle)
        );

        //glDrawArrays(GL_TRIANGLES, 0, 3);
    }
}
