#include "DebugFunctions.h"

namespace debugFunctions {

	void displayData(vector<Money*> moneys) {
		cout << "Money" << endl;
		for (vector<Money*>::iterator it = moneys.begin(); it != moneys.end() ; ++it) {
			cout << (*it)->getString() << endl;
		}
	}
	void displayDataPopulation(vector<Person*> population) {
		for (vector<Person*>::iterator it = population.begin(); it != population.end() ; ++it) {
			cout << (*it)->getString() << endl;
		}
	}
	void displayDataPopulation(vector<Aristocrat*> population) {
		for (vector<Aristocrat*>::iterator it = population.begin(); it != population.end() ; ++it) {
			cout << (*it)->getString() << endl;
		}
	}
	void displayDataPopulation(vector<Villager*> population) {
		for (vector<Villager*>::iterator it = population.begin(); it != population.end() ; ++it) {
			cout << (*it)->getString() << endl;
		}
	}
	void displayDataKingdoms(vector<Kingdom*> kingdoms) {
		for (vector<Kingdom*>::iterator it = kingdoms.begin(); it != kingdoms.end(); ++it) {
			cout << "\nKingdom "<< (*it)->getName() << endl;
			displayDataPopulation((*it)->getPopulation());
		}
	}
	void displayDataEntitiesPerson(vector<Entity*> *entities) {
		for (vector<Entity*>::iterator it = entities->begin(); it != entities->end() ; ++it) {
			cout << (*it)->getString() << endl;
		}
	}
}
