#include "mRandom.h"

bool alreadyIn(int * tab, int count, int val) {
	for (int i = 0; i < count; ++i) {
		if(tab[i] == val)
			return true;
	}
	return false;
}

namespace mRandom {

	int getRandomInt(unsigned int inf, unsigned int sup) {
		return (rand() % (sup - inf)) + inf;
	}

	double getRandomDouble(double inf, double sup) {
		int somme = 0;
		for (int i = 0; i < NBR_CHIFFRE_FOR_DOUBLE; ++i) {
			somme += getRandomInt(inf, sup);
		}
		return somme / NBR_CHIFFRE_FOR_DOUBLE;
	}

	string getRandomName(unsigned int size) {
		string res("");
		for (unsigned int i = 0; i < size; ++i) {
			char c = getRandomInt(LETTRE_MIN, LETTRE_MAX+1);
			res += c+"";
		}
		string t = res;
		return res;
	}

	int * getUniqueIntegers(int inf, int sup, int count) {
		assert(sup-inf > count);
		int * res = new int(count);
		memset(res, inf-1, count);
		for (int i = 0; i < count; ++i) {
			int val = getRandomInt(inf, sup);
			while(alreadyIn(res, count, val)) {
				val = getRandomInt(inf, sup);
			}
			res[i] = val;
		}
		return res;
	}
};