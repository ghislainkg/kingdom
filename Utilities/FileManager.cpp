#include "FileManager.h"

namespace FileManager {

	string readFile(string const & filename) {
		ifstream stream (filename);

		if(!stream.is_open()) {
			cout << "Fail to open shader file" << endl;
			exit(1);
		}

		string line;
		string content;
		while(getline(stream, line)) {
			content += (line + "\n");
		}

		return content;
	}
}