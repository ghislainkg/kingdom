#ifndef M_RANDOM_
#define M_RANDOM_

#include <cstdlib>
#include <time.h>
#include <math.h>
#include <string>
#include <iostream>
#include <cstring>
#include <cassert>

#define NBR_CHIFFRE_FOR_DOUBLE 10
#define LETTRE_MIN 65
#define LETTRE_MAX 90

using namespace std;

namespace mRandom {

	int getRandomInt(unsigned int inf, unsigned int sup);

	double getRandomDouble(double inf, double sup);

	string getRandomName(unsigned int size);

	int * getUniqueIntegers(int inf, int sup, int count);
}

#endif