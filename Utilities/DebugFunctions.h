#ifndef _DEBUG_FUNCTIONS_
#define _DEBUG_FUNCTIONS_

#include <string>
#include <iostream>
#include <vector>

#include "../Life/Interaction.h"
#include "../Initialisation/Entity.h"
#include "../DataClass/Person.h"
#include "../DataClass/Aristocrat.h"
#include "../DataClass/Villager.h"
#include "../DataClass/PorteFeuille.h"
#include "../DataClass/Money.h"
#include "../DataClass/Kingdom.h"

using namespace std;

namespace debugFunctions {

	void displayData(vector<Money*> moneys);
	void displayDataPopulation(vector<Person*> population);
	void displayDataPopulation(vector<Aristocrat*> population);
	void displayDataPopulation(vector<Villager*> population);
	void displayDataKingdoms(vector<Kingdom*> kingdoms);
	void displayDataEntitiesPerson(vector<Entity*> *entities);
}

#endif