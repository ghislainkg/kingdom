#ifndef _FILE_MANAGER_
#define _FILE_MANAGER_

#include <iostream>
#include <fstream>
#include <string>

using namespace std;

namespace FileManager {
	string readFile(string const & filename);
}

#endif