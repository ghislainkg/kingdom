#include <iostream>
#include <time.h>
#include <iostream>
#include <fstream>
#include <string>
#include <thread>

#include "Life/Vie.h"

#include "graphics/Drawing.h"
#include "graphics/EntityDrawing.h"
#include "graphics/OpenglUtils.h"
#include "Initialisation/WorldInitializer.h"
#include "Utilities/FileManager.h"

using namespace std;
using namespace EntityDrawing;
using namespace FileManager;

Vie v;
void appel(){
	v.live(1000);
}

int main(int argc, char const *argv[]) {

	srand(time(0));

	cout << "Salut !" << endl;
	v = Vie(1, 50, 20, 500);

	WorldInitializer *  w = v.getWorld();
	vector<Entity*> * entities = w->getMonde();

//**************************************************************************	

	GLFWwindow * window = startOpenGL(600, 300, "Kingdom");
	displayOpenGLVersion();

//**************************************************************************
	vector<ShaderSource> shaders_sources = vector<ShaderSource>();
	string filename = "shaders/squarevertexshader.shader";
	string sourcevertexshader = readFile(filename);
	ShaderSource ssv = {
		GL_VERTEX_SHADER,
		sourcevertexshader
	};
	shaders_sources.push_back(ssv);

	filename = "shaders/squarefragmentshader.shader";
	string sourcefragmentshader = readFile(filename);
	ShaderSource ssf = {
		GL_FRAGMENT_SHADER,
		sourcefragmentshader
	};
	shaders_sources.push_back(ssf);
	unsigned int shader_program_id = createShadingProgram(shaders_sources);

	glUseProgram(shader_program_id);

	errorClearGLError();
	setSquareAttributes();
	errorCheckGLError();

//**************************************************************************

	thread m(appel);

	while (!glfwWindowShouldClose(window)) {
		errorClearGLError();
		glClear(GL_COLOR_BUFFER_BIT);
		errorCheckGLError();

		drawEntities(entities);

		vector<Entity*> p = *entities;

		errorClearGLError();
		setSquareBuffer(p[0]->getSquare());
		errorCheckGLError();

		errorClearGLError();
		setSquareAttributes();
		errorCheckGLError();

		errorClearGLError();
		drawSquare();
		errorCheckGLError();
		
		errorClearGLError();
		glfwSwapBuffers(window);
		errorCheckGLError();

		errorClearGLError();
        glfwPollEvents();
        errorCheckGLError();
	}

    glfwTerminate();

    v.displayAllScores();

	return 0;
}