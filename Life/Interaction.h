#ifndef _INTERACTION_
#define _INTERACTION_

#include <string>
#include <iostream>

#include "../DataClass/Person.h"
#include "../DataClass/PorteFeuille.h"
#include "../Utilities/mRandom.h"

using namespace mRandom;
using namespace std;

typedef enum Action {
	COMMERCE = 0,
	CHANGER_CAMPT = 1,
} Action;

/*Une intéraction se fait entre deux Person et est éssentiellement un échange commercial*/
class Interaction {
public:
	Interaction(Person * person_x_, Person * person_y_);

	void interact();

	static constexpr double max_echange_valeur = 2.0;

	string getStringOfAction(Action a) const;

	string getString() const;

	ostream & operator<<(ostream & stream);

private:
	Person * person_x;
	Person * person_y;

	int action;

	void echangerMoney(Person * from, Person * to, double valeur);
};

#endif