#ifndef _VIE_
#define _VIE_

#include <math.h>
#include <vector>
#include <thread>

#include "Interaction.h"
#include "../Initialisation/WorldInitializer.h"
#include "../Initialisation/Entity.h"
#include "../DataClass/Person.h"
#include "../Utilities/mRandom.h"
#include "../Utilities/DebugFunctions.h"

using namespace debugFunctions;

struct Score {
	float valeur;
	long int kingdom;
};

class Vie {
public:
	Vie(unsigned int nbrKingdom,
		unsigned int population_kindom_,
		double pourcentage_aristo_,
		double valeurTotal);
	Vie();

	void setCloseDistance(float close_distance_);
	float getCloseDistance();

	void live(unsigned int nbr_tours);

	WorldInitializer * getWorld();

	Score * getScores();
	string getStringOfScore(Score s);
	void displayScores(Score *s, int count);
	void displayAllScores();

private:
	WorldInitializer * world;
	float close_distance = 0.1;

	void deplaceEntity(Entity * entity);
	bool sontProche(Entity * entity_x, Entity * entity_y);

	void deplaceEveryThing(vector<Entity*> *monde);
	void faireDesInteractions(vector<Entity*> *monde);

	void trancheDeVie();

	// La grande boucle
	void everyDay(unsigned int nbr_tours);
};

#endif