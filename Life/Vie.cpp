#include "Vie.h"

Vie::Vie(unsigned int nbrKingdom,
	unsigned int population_kindom_,
	double pourcentage_aristo_,
	double valeurTotal) {
	world = new WorldInitializer(nbrKingdom, population_kindom_, pourcentage_aristo_, valeurTotal);
}

Vie::Vie() {}

void Vie::setCloseDistance(float close_distance_) {
	this->close_distance = close_distance_;
}

float Vie::getCloseDistance() {
	return this->close_distance;
}

void Vie::live(unsigned int nbr_tours) {
	everyDay(nbr_tours);
}

WorldInitializer * Vie::getWorld() {
	return this->world;
}

Deplacement getRandomDeplacement() {
	int choix = getRandomInt(0, 4);
	switch(choix) {
		case 0:
			return Deplacement::UP;
		case 1:
			return Deplacement::DOWN;
		case 2:
			return Deplacement::RIGHT;
		case 3:
			return Deplacement::LEFT;
		default:
			cout << "Not a Deplacement" << endl;
			exit(1);
	}
}

unsigned int getRandomSpeed() {
	int speed_signed = getRandomInt(0, Entity::max_speed);
	return (unsigned int) speed_signed;
}

void Vie::deplaceEntity(Entity * entity) {
	Deplacement d = getRandomDeplacement();
	float speed = 0.1;//getRandomSpeed();
	entity->deplacer(d, speed);
}

float calculDistance_aux(float x1, float y1, float x2, float y2) {
	return sqrt(pow(y2 - y1, 2) + pow(x2 - x1, 2));
}
float calculDistance(Entity * entity_x, Entity * entity_y) {
	return calculDistance_aux(entity_x->getX(), entity_x->getY(), entity_y->getX(), entity_y->getY());
}
bool Vie::sontProche(Entity * entity_x, Entity * entity_y) {
	float distance = calculDistance(entity_x, entity_y);
	if(distance <= close_distance)
		return true;
	else
		return false;
}

void Vie::deplaceEveryThing(vector<Entity*> * monde) {
	for (vector<Entity*>::iterator it = monde->begin(); it != monde->end() ; ++it){
		deplaceEntity(*it);
	}
}

void Vie::faireDesInteractions(vector<Entity*> *monde) {
	for (vector<Entity*>::iterator iti = monde->begin(); iti != monde->end(); ++iti) {
		for (vector<Entity*>::iterator itk = monde->begin(); itk != monde->end(); ++itk) {
			Entity * entity_i = (*iti);
			Entity * entity_k = (*itk);
			if(iti != itk && sontProche(entity_i, entity_k)) {
				Interaction interaction = Interaction(entity_i->getContenu(), entity_k->getContenu());
				interaction.interact();
			}
		}
	}
}

void Vie::trancheDeVie() {
	vector<Entity*> * monde = world->getMonde();
	deplaceEveryThing(monde);
	faireDesInteractions(monde);
}

// La grande boucle
void Vie::everyDay(unsigned int nbr_tours) {
	using namespace literals::chrono_literals;
	
	for (unsigned int i = 0; i < nbr_tours; ++i) {
		trancheDeVie();
		this_thread::sleep_for(1s);
	}
}

Score * Vie::getScores() {
	WorldInitializer * w = getWorld();
	vector<Kingdom*> kds = w->getKingdoms();
	Score * scores = new Score[kds.size()];
	int is = 0;
	for (vector<Kingdom*>::iterator itk = kds.begin(); itk != kds.end() ; ++itk) {
		Kingdom * kd = (*itk);
		scores[is].kingdom = kd->getId();

		vector<Person *> pop = kd->getPopulation();
		for (vector<Person *>::iterator itp = pop.begin(); itp != pop.end() ; ++itp) {
			Person * person = (*itp);
			scores[is].valeur += person->getPorteFeuille().getValeur();
		}

		is++;
	}
	return scores;
}

string Vie::getStringOfScore(Score s) {
	return "kingdom : " + to_string(s.kingdom) + " score = " + to_string(s.valeur); 
}

void Vie::displayScores(Score *s, int count) {
	for (int i = 0; i < count; ++i) {
		cout << getStringOfScore(s[i]) << endl;
	}
}

void Vie::displayAllScores() {
	displayScores(this->getScores(), getWorld()->getNbrKingdom());
}

// TODO : Les scores parfois negatif, parfois beaucoup trop grand