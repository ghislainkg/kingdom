#include "Interaction.h"

Interaction::Interaction(Person * person_x_, Person * person_y_) :
	person_x(person_x_), person_y(person_y_) {

}

void Interaction::echangerMoney(Person * from, Person * to, double valeur) {
	PorteFeuille pfrom = from->getPorteFeuille();
	PorteFeuille pto = to->getPorteFeuille();

	pto.getValeurFrom(pfrom, valeur);
	
	from->setPorteFeuille(pfrom);
	to->setPorteFeuille(pto);
}

void Interaction::interact() {
	// Pour le moment, on ne prend en compte que COMMERCE
	this->action = getRandomInt(0, 1);
	switch(this->action) {
		case COMMERCE:
			{
				double valeur = getRandomDouble(0.0, max_echange_valeur + 5.0);
				int sens = getRandomInt(0, 2);
				if(sens == 0)
					echangerMoney(this->person_x, this->person_y, valeur);
				else
					echangerMoney(this->person_y, this->person_x, valeur);
			}
			break;
		case CHANGER_CAMPT:
			break;
		default:
			cout << "Not an Action" << endl;
			exit(1);
	}
}

string Interaction::getStringOfAction(Action a) const {
	switch(a) {
		case COMMERCE:
			return "COMMERCE";
		case CHANGER_CAMPT:
			return "CHANGER_CAMPT";
		default:
			cout << "Not an Action" << endl;
			exit(1);
	}
}

string Interaction::getString() const {
	return "( Interaction : person_x = " + person_x->getString() + " person_y = " + person_y->getString() + " action = " + to_string(this->action) + ")";
}

ostream & Interaction::operator<<(ostream & stream) {
	stream << getString();
	return stream;
}