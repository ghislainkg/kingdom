#version 420 core

layout(location = 0) in vec2 position;

layout(location = 1) in float col;

out vec3 mColor;

float r;
float g;
float b;

void main() {
	gl_Position = vec4(position.x, position.y, 0.0, 1.0);
	if (col > 1.0) {
		r = 1.0;
		if (col > 2.0) {
			g = 1.0;
			if (col > 3.0) {
				b = 1.0;
			}
			else {
				b = col - 2.0;
			}
		}
		else {
			g = col - 1.0;
			b = 0.0;
		}
	}
	else {
		r = col;
		g = 0.0;
		b = 0.0;
	}
	mColor = vec3(r, g, b);
	//mColor = vec3(1.0, 1.0, 1.0);
}