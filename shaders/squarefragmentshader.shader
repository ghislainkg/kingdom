#version 420 core

in vec3 mColor;
out vec4 FragColor;

void main() {
	FragColor = vec4(mColor.x, mColor.y, mColor.z, 0.0);
}