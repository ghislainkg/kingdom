# directories
data=DataClass/
obj=obj/
init=Initialisation/
util=Utilities/
life=Life/
graphics=graphics/

# compilation options
option= -Wall -g
#-save-temps

# libraries
lib= -lGL -lGLEW -lGLU `pkg-config --static --libs glfw3` -lpthread

all : kingdom

kingdom : $(obj)FileManager.o $(obj)EntityDrawing.o $(obj)Drawing.o $(obj)OpenglUtils.o $(obj)DebugFunctions.o $(obj)mRandom.o $(obj)WorldInitializer.o $(obj)Interaction.o $(obj)Vie.o $(obj)main.o $(obj)Kingdom.o $(obj)PorteFeuille.o $(obj)Money.o $(obj)Person.o $(obj)Aristocrat.o $(obj)Villager.o $(obj)Entity.o
	g++ $(option) $(obj)*.o -o kingdom $(lib)

$(obj)Kingdom.o : $(data)Kingdom.cpp
	g++ $(option) -c $(data)Kingdom.cpp -o $(obj)Kingdom.o

$(obj)PorteFeuille.o : $(data)PorteFeuille.cpp
	g++ $(option) -c $(data)PorteFeuille.cpp -o $(obj)PorteFeuille.o

$(obj)Money.o : $(data)Money.cpp
	g++ $(option) -c $(data)Money.cpp -o $(obj)Money.o

$(obj)Person.o : $(data)Person.cpp
	g++ $(option) -c $(data)Person.cpp -o $(obj)Person.o

$(obj)Aristocrat.o : $(data)Aristocrat.cpp
	g++ $(option) -c $(data)Aristocrat.cpp -o $(obj)Aristocrat.o

$(obj)Villager.o : $(data)Villager.cpp
	g++ $(option) -c $(data)Villager.cpp -o $(obj)Villager.o

$(obj)WorldInitializer.o : $(init)WorldInitializer.cpp $(util)mRandom.cpp
	g++ $(option) -c $(init)WorldInitializer.cpp -o $(obj)WorldInitializer.o

$(obj)Entity.o : $(init)Entity.cpp
	g++ $(option) -c $(init)Entity.cpp -o $(obj)Entity.o

$(obj)mRandom.o : $(util)mRandom.cpp
	g++ $(option) -c $(util)mRandom.cpp -o $(obj)mRandom.o

$(obj)DebugFunctions.o : $(util)DebugFunctions.cpp
	g++ $(option) -c $(util)DebugFunctions.cpp -o $(obj)DebugFunctions.o

$(obj)Vie.o : $(life)Vie.cpp
	g++ $(option) -c $(life)Vie.cpp -o $(obj)Vie.o

$(obj)Interaction.o : $(life)Interaction.cpp
	g++ $(option) -c $(life)Interaction.cpp -o $(obj)Interaction.o

$(obj)Drawing.o : $(graphics)Drawing.cpp
	g++ $(option) -c $(graphics)Drawing.cpp -o $(obj)Drawing.o $(lib)

$(obj)OpenglUtils.o : $(graphics)OpenglUtils.cpp
	g++ $(option) -c $(graphics)OpenglUtils.cpp -o $(obj)OpenglUtils.o $(lib)

$(obj)EntityDrawing.o : $(graphics)EntityDrawing.cpp
	g++ $(option) -c $(graphics)EntityDrawing.cpp -o $(obj)EntityDrawing.o $(lib)

$(obj)FileManager.o : $(util)FileManager.cpp
	g++ $(option) -c $(util)FileManager.cpp -o $(obj)FileManager.o

$(obj)main.o : main.cpp
	g++ $(option) -c main.cpp -o $(obj)main.o

clean :
	rm -rf $(obj)*.o kingdom