#ifndef _ENTITY_
#define _ENTITY_

#include <string>
#include <iostream>
#include <cmath>

#include "../DataClass/Person.h"
#include "../graphics/Drawing.h"
#include "../Utilities/mRandom.h"

using namespace std;

// Une Entity est la representation d'une Person dans l'espace

// Deja une Entity a une position et peut se deplacer.
// Elle ne peut se deplacer que de 4 facons
typedef enum Deplacement {
	UP = 0, // Haut
	DOWN = 1, // Bas
	RIGHT = 2, // Droite
	LEFT = 3 // Gauche
} Deplacement;

class Entity {
public:

	// Toutes les Entity ont la meme epaisseur dans l'espace
	static constexpr float EPAISSEUR = 0.05;
	// Et la meme vitesse maximale de deplacement
	static constexpr float max_speed = 0.01;

	// On peut creer une Entity avec une position de depart (x_, y_)
	// Et un contenu (une Person)
	Entity(float x_, float y_, Person * contenu_);

	// 
	Square getSquare() const;
	Person * getContenu() const;

	void setContenu(Person * contenu_);

	void deplacer(Deplacement deplacement, float speed);

	string getStringForDeplacement(Deplacement d) const;

	float getX() const;
	float getY() const;

	string getString() const;

	ostream & operator<<(ostream & stream);

private:
	Person * contenu;
	Square square;

	void checkPositionAxe(Square sq);
	void setEntityColor();
};

#endif