#include "WorldInitializer.h"

WorldInitializer::WorldInitializer(unsigned int nbr_kingdom_,
	unsigned int population_kindom_,
	double pourcentage_aristo_,
	double valeurTotal_) {

	nbr_kingdom = nbr_kingdom_;
	population_kindom = population_kindom_;
	pourcentage_aristo = pourcentage_aristo_;
	valeurTotal = valeurTotal_;
	
	this->monde = new vector<Entity*>();

	vector<Money*> moneys = generateMoney(nbr_kingdom);

	this->money_universelle = moneys[0];
	this->kingdoms = generateKindoms(nbr_kingdom, moneys);
	
	this->monde = generateEntitiesForKindoms(this->kingdoms);

}

vector<Entity*> * WorldInitializer::getMonde() const {
	return this->monde;
}
vector<Kingdom*> WorldInitializer::getKingdoms() const {
	return this->kingdoms;
}

vector<Entity*> * WorldInitializer::generateEntitiesForKindoms(vector<Kingdom*> kingdoms) {
	vector<Entity*> * res = new vector<Entity*>();
	for (vector<Kingdom*>::iterator it = kingdoms.begin(); it != kingdoms.end(); ++it) {
		vector<Entity*> aux = generateEntities((*it)->getPopulation());
		res->insert(res->begin(), aux.begin(), aux.end());
	}
	return res;
}

Aristocrat * WorldInitializer::getKing(Kingdom * kingdom, Money *money) {
	string name = getRandomName(name_size);
	int age = getRandomInt(min_age_king, max_age_king);
	long int id_kingdom = kingdom->getId();
	Grade grade = Grade::KING;
	return new Aristocrat(name, age, id_kingdom, money, grade);
}

void WorldInitializer::ajouterPersonToKingdom(vector<Person*> persons, Kingdom * kingdom) {
	for (vector<Person*>::iterator it = persons.begin(); it != persons.end(); ++it) {
		kingdom->addPerson(*it);
	}
}

int * getIds(int count) {
	int * res = new int[count];
	for (int i = 1; i <= count; ++i) {
		res[i-1] = i;
	}
	return res;
}

vector<Kingdom*> WorldInitializer::generateKindoms(unsigned int count, vector<Money*> moneys) {
	vector<Kingdom*> res = vector<Kingdom*>();

	int * ids = getIds(count);

	for (unsigned int i = 0; i < count; ++i) {
		string name = getRandomName(name_size);

		Kingdom *kingdom = new Kingdom(name, moneys[i], ids[i]);
		Aristocrat * king = getKing(kingdom, moneys[i]);
		kingdom->setKing(king);
		
		vector<Person*> population = generatePopulation(kingdom, moneys[i], this->population_kindom, this->pourcentage_aristo);
		ajouterPersonToKingdom(population, kingdom);

		res.push_back(kingdom);
		
	}
	return res;
}

int WorldInitializer::getNbrAristocrat(int pourcentage_aristo, int count) {
	return pourcentage_aristo * count / 100;
}
vector<Person*> WorldInitializer::generatePopulation(Kingdom * kingdom, Money *money, unsigned int count, int pourcentage_aristo) {
	
	vector<Person*> res = vector<Person*>();
	int n_aristo = getNbrAristocrat(pourcentage_aristo, count);
	vector<Aristocrat*> aristos = generateAristocrat(kingdom, money, n_aristo);
	vector<Villager*> villagers = generateVillager(kingdom, money, count - n_aristo);

	res.insert( res.begin(), aristos.begin(), aristos.end() );
	res.insert( res.begin(), villagers.begin(), villagers.end() );

	return res;
}

Grade WorldInitializer::getRandomGrade() {
	int choix = getRandomInt(0, 3);
	switch(choix) {
		case 0:
			return KING;
		case 1:
			return COMPTE;
		case 2:
			return REGENT;
		default:
			cout << "Not a Grade" << endl;
			exit(1);
	}
}

vector<Aristocrat*> WorldInitializer::generateAristocrat(Kingdom * kingdom, Money * money, unsigned int count) {
	vector<Aristocrat*> res = vector<Aristocrat*>();
	for (unsigned int i = 0; i < count; ++i) {
		Grade grade = getRandomGrade();
		int age = getRandomInt(0, max_age+1);
		string name = getRandomName(name_size);

		Aristocrat * aux = new Aristocrat(name, age, kingdom->getId(), money, grade);
		PorteFeuille p = creerPorteFeuille(money);
		aux->setPorteFeuille(p);
		
		res.push_back(aux);
	}
	return res;
}

Work WorldInitializer::getRandomWork() {
	int choix = getRandomInt(0, 4);
	switch(choix) {
		case 0:
			return AGRICULTEUR;
		case 1:
			return MARCHANT;
		case 2:
			return CHARPENTIER;
		case 3:
			return ENSEIGNANT;
		default:
			cout << "Not a Work"<< endl;
			exit(1);
	}
}

vector<Villager*> WorldInitializer::generateVillager(Kingdom * kingdom, Money * money, unsigned int count) {
	vector<Villager*> res = vector<Villager*>();
	for (unsigned int i = 0; i < count; ++i) {
		Work work = getRandomWork();
		int age = getRandomInt(0, max_age+1);
		string name = getRandomName(name_size);
		
		Villager * aux = new Villager(name, age, kingdom->getId(), money, work);
		PorteFeuille p = creerPorteFeuille(money);
		aux->setPorteFeuille(p);


		res.push_back(aux);
	}
	return res;
}
vector<Money*> WorldInitializer::generateMoney(int count) {
	vector<Money*> res = vector<Money*>();
	for (int i = 0; i < count; ++i) {
		string name = getRandomName(name_size);
		double valeur = getRandomDouble(start_min_valeurmarcher, start_max_valeurmarcher);
		Money * aux = new Money(name, valeur);
		res.push_back(aux);
	}
	return res;
}

vector<Entity*> WorldInitializer::generateEntities(vector<Person*> population) {
	vector<Entity*> res = vector<Entity*>();
	for (vector<Person *>::iterator it = population.begin(); it != population.end() ; ++it) {
		float x = getRandomInt(0, limit_monde+1);
		float y = getRandomInt(0, limit_monde+1);
		Entity * aux = new Entity(x, y, *it);
		res.push_back(aux);
	}
	return res;
}

PorteFeuille WorldInitializer::creerPorteFeuille(Money * money) {
	double valeur_money_univ;
	
	if(this->valeurTotal == 0.0) {
		valeur_money_univ = 0.0;
	}
	else {
		valeur_money_univ = getRandomDouble(0.0, init_max_portefeuille_value);
	}
	if(this->valeurTotal < valeur_money_univ) {
		valeur_money_univ = this->valeurTotal;
	}

	double valeur = Money::convertir(*(this->money_universelle), *money, valeur_money_univ);

	PorteFeuille res = PorteFeuille(money, valeur);
	this->valeurTotal -= valeur_money_univ;
	
	return res;
}

unsigned int WorldInitializer::getNbrKingdom() {
	return nbr_kingdom;
}
unsigned int WorldInitializer::getPopulationKindom() {
	return population_kindom;
}
double WorldInitializer::getPourcentageAristo() {
	return pourcentage_aristo;
}
double WorldInitializer::getValeurTotal() {
	return valeurTotal;
}