#include "Entity.h"

float getColor(int a) {
	float step = 0.05;
	float res = 0.0;
	for (int i = 0; i < a; ++i) {
		res += step;
		if(res > 3) {
			res = step;
		}
	}
	return res;
}

void Entity::setEntityColor() {
	float couleur = getColor(this->contenu->getPorteFeuille().getValeur());
	square.ca = couleur;
	square.cb = couleur;
	square.cc = couleur;
	square.cd = couleur;
}

Entity::Entity(float x_, float y_, Person * contenu_) : 
	contenu(contenu_){

	x_ = 10.0 / (x_);
	y_ = 10.0 / (y_);

	square.ax = x_ - EPAISSEUR;
	square.ay = y_ + EPAISSEUR;
	
	square.bx = x_ + EPAISSEUR;
	square.by = y_ + EPAISSEUR;
	
	square.cx = x_ + EPAISSEUR;
	square.cy = y_ - EPAISSEUR;
	
	square.dx = x_ - EPAISSEUR;
	square.dy = y_ - EPAISSEUR;

	setEntityColor();
};

Square Entity::getSquare() const {
	return square;
};

Person * Entity::getContenu() const {
	return this->contenu;
};
void Entity::setContenu(Person * contenu_) {
	this->contenu = contenu_;
};

void Entity::checkPositionAxe(Square sq) {
	if(sq.ax > 1.0 || sq.bx > 1.0 || sq.cx > 1.0 || sq.dx > 1.0) {
		square.ax -= 1.0;
		square.bx -= 1.0;
		square.cx -= 1.0;
		square.dx -= 1.0;
	}
	if(sq.ax < -1.0 || sq.bx < -1.0 || sq.cx < -1.0 || sq.dx < -1.0) {
		square.ax += 1.0;
		square.bx += 1.0;
		square.cx += 1.0;
		square.dx += 1.0;
	}
	
	if(sq.ay > 1.0 || sq.by > 1.0 || sq.cy > 1.0 || sq.dy > 1.0) {
		square.ay -= 1.0;
		square.by -= 1.0;
		square.cy -= 1.0;
		square.dy -= 1.0;
	}
	if(sq.ay < -1.0 || sq.by < -1.0 || sq.cy < -1.0 || sq.dy < -1.0) {
		square.ay += 1.0;
		square.by += 1.0;
		square.cy += 1.0;
		square.dy += 1.0;
	}
};

void Entity::deplacer(Deplacement deplacement, float speed) {
	switch(deplacement) {
		case UP :
			square.ay += speed;
			square.by += speed;
			square.cy += speed;
			square.dy += speed;
			break;
		case DOWN :
			square.ay -= speed;
			square.by -= speed;
			square.cy -= speed;
			square.dy -= speed;
			break;
		case RIGHT :
			square.ax += speed;
			square.bx += speed;
			square.cx += speed;
			square.dx += speed;
			break;
		case LEFT :
			square.ax -= speed;
			square.bx -= speed;
			square.cx -= speed;
			square.dx -= speed;
			break;
		default :
			;
	}
	checkPositionAxe(square);

	setEntityColor();
};

string Entity::getStringForDeplacement(Deplacement d) const {
	switch(d) {
		case UP :
			return "UP";
		case DOWN :
			return "DOWN";
		case RIGHT :
			return "RIGHT";
		case LEFT :
			return "LEFT";
		default :
			cout << "NOT A Deplacement";
			exit(1);
	}
};

float Entity::getX() const {
	return square.ax + EPAISSEUR;
};
float Entity::getY() const {
	return square.ay - EPAISSEUR;
};

string Entity::getString() const {
	return "( Entity : x = " + to_string(getX()) + " y = " + to_string(getY()) + " contenu = " + ((Person*) contenu)->getString() + " )";
};

ostream & Entity::operator<<(ostream & stream) {
	stream << getString();
	return stream;
};