#ifndef _WORLD_INITIALIZER_
#define _WORLD_INITIALIZER_

#include <iostream>
#include <string>
#include <vector>

#include "../DataClass/Person.h"
#include "../DataClass/PorteFeuille.h"
#include "../DataClass/Aristocrat.h"
#include "../DataClass/Villager.h"
#include "../DataClass/Kingdom.h"
#include "../Utilities/mRandom.h"
#include "Entity.h"
#include "../Utilities/DebugFunctions.h"

using namespace std;
using namespace mRandom;
using namespace debugFunctions;

class WorldInitializer {

public:
	WorldInitializer(unsigned int nbr_kingdom_,
		unsigned int population_kindom_,
		double pourcentage_aristo_,
		double valeurTotal);

	static const int valeur_sup_ids = 100;
	static const int valeur_inf_ids = 1;

	static constexpr double start_max_valeurmarcher = 5.0;
	static constexpr double start_min_valeurmarcher = 0.0;

	static const int limit_monde = 500;

	static const int name_size = 10; // 26^10 possibilités

	static const int max_age = 200;
	static const int min_age_king = 20;
	static const int max_age_king = 200;

	static const int max_nbr_persons = 10;
	static const int min_nbr_persons = 5;
	static constexpr double init_max_portefeuille_value = 10.0;

	vector<Entity*> * getMonde() const;
	vector<Kingdom*> getKingdoms() const;

	unsigned int getNbrKingdom();
	unsigned int getPopulationKindom();
	double getPourcentageAristo();
	double getValeurTotal();

private:
	unsigned int nbr_kingdom;
	unsigned int population_kindom;
	double pourcentage_aristo;
	double valeurTotal;

	vector<Entity*> * monde; // Toutes les entités du monde (Person)
	vector<Kingdom*> kingdoms;
	Money * money_universelle;

	vector<Kingdom*> generateKindoms(unsigned int count, vector<Money*> moneys);
	vector<Person*> generatePopulation(Kingdom * kingdom, Money *money, unsigned int count, int pourcentage_aristo);
	vector<Aristocrat*> generateAristocrat(Kingdom * kingdom, Money * money, unsigned int count);
	vector<Villager*> generateVillager(Kingdom * kingdom, Money * money, unsigned int count);
	vector<Money*> generateMoney(int count);

	vector<Entity*> generateEntities(vector<Person*> population);

	Work getRandomWork();
	Grade getRandomGrade();
	int getNbrAristocrat(int pourcentage_aristo, int count);
	void ajouterPersonToKingdom(vector<Person*> persons, Kingdom * kingdom);
	Aristocrat * getKing(Kingdom * kingdom, Money *money);
	vector<Entity*> * generateEntitiesForKindoms(vector<Kingdom*> kingdoms);

	PorteFeuille creerPorteFeuille(Money * money);

};

#endif