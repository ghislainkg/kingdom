#ifndef _KINGDOM_
#define _KINGDOM_

#include <iostream>
#include <string>
#include <vector>
#include <time.h>

#include "Person.h"
#include "Money.h"
#include "Aristocrat.h"
#include "Villager.h"

// Definition de ce qu'est un royaume (Kingdom) dans ce monde

using namespace std;

class Kingdom {

public:

	// CONSTRUCTEURS ********************************************
	// On peut creer un royaume avec un nom, une money
	// ainsi qu'un id (qui devra etre unique pour chaque Kingdom)
	// (Qui sera utilisee par le Person peuplant ce Kingdom)
	// Dans ce cas, pas de roi (king = NULL)
	Kingdom(string name_, Money * money_, int id_);

	// On peut aussi le faire a rajoutant un roi (king_)
	Kingdom(string name_, Money * money_, int id_, Aristocrat * king_);

	// GETTERS *************************************************

	// On peut connaitre le nom (name) d'un Kingdom
	string getName() const;

	// son id	
	long int getId();

	// Ainsi que son roi (king)
	Aristocrat * getKing() const;

	// Et sa money
	Money * getMoney() const;

	// Meme le nombre de Person de ce Kingdom
	int getNbrPersons() const;

	
	
	// Et toute sa population
	vector<Person *> getPopulation() const;
	
	// SETTERS **************************************************

	// On peut changer le king d'un Kingdom
	// Il faut que le nouveau roi soi effectivement un roi
	// (king_->getGrade() == Grade::KING) (Grade est defini dans Aristocrat.h)
	void setKing(Aristocrat * king_);
	
	// On peut changer la money d'un Kingdom
	// La money de toutes les PorteFeuille des Person du Kingdom est mise a jour 
	void setMoney(Money * money_);

	// On peut bien-sur changer le nom d'un Kingdom
	void setName(string const & name_);

	// On peut supprimer une Person d'un Kingdom
	// Si on le fait, le nbr_persons--
	// et le champ kingdom_id de cette Person est mis a Person::NO_KINGDOM
	void removePerson(Person * person_);

	// On Peut aussi ajouter une Person a un Kingdom
	// Si on le fait, nbr_persons++
	// et le champ kingdom_id de cette Person est mis a this->id
	// Et son PorteFeuille est converti en porteFeuille de la money du kingdom
	// Son champ kingdom_id = this->id
	void addPerson(Person * person_);

	// AUTRES METHODES ********************************************

	// On peut avoir des informations sur un kingdom
	string getString() const;

	// On peut aussi l'ecrire dans un flux de sortie
	ostream & operator<<(ostream & stream);

private:

	// Un kingdom possede un id
	long int id;

	// un nom
	string name;

	// Un roi
	Aristocrat * king;

	// Une money qui est attribuee a tous les porteFeuille des Person du Kingdom
	Money * money;

	// un nombre de Person
	unsigned int nbr_persons;

	// et bien-sur un ensemble de Person
	vector<Person *> population;
};

#endif