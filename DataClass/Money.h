#ifndef _MONEY_
#define _MONEY_

#include <iostream>
#include <string>

// Definition de ce qu'est une Money dans ce monde

using namespace std;

class Money {
public:

	// CONSTRUCTEURS ******************************************
	
	// On peut creer une Money juste avec un nom, alors sa valeur_marcher = 1
	Money(string name_);

	// Ou on peut la creer avec un nom et sa valeur sur le marcher
	Money(string name_, double valeur_marcher_);

	// GETTERS ***************************************************
	
	// On peut connaitre le nom d'une Money
	string getName() const;

	// On peut aussi connaitre sa valeur sur le marcher
	double getValeur() const;

	// SETTERS *************************************************

	// On peut changer son nom
	void setName(string const & name_);

	// Et aussi sa valeur
	void setValeur(double const & valeur_marcher_);

	// AUTRES METHODES *********************************************

	// On peut avoir des informations sur une money 
	string getString() const;

	// On peut convertir une quantite de valeur d'une Money from_
	// en une quantite de valeur d'une Money to_
	static double convertir(Money from_, Money to_, double quantite_);
	
	// On peut bien-sur comparer des Money
	// en les jugant sur leur valeur sur le marcher
	bool operator=(Money const & money_);
	bool operator>(Money const & money_);
	bool operator<(Money const & money_);

	// On peut ecrire une Money dans un flux de sortie
	ostream & operator<<(ostream & stream);

private:

	// Un money possede un nom (name)
	string name;

	// Et une valeur sur le marcher
	double valeur_marcher;

};


#endif