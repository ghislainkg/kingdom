#ifndef _PERSON_
#define _PERSON_

#include <iostream>
#include <string>

#include "PorteFeuille.h"
#include "Money.h"

// Definition de ce qu'est une personne (alias Person)

using namespace std;

class Person {
public:

	// CONSTRUCTEURS *******************************************

	// On peut creer une Person avec un nom (name_), un age (age_),
	// un identifiant pour le royaume auquel il appartient,
	// Et une money (money_) pour ses echanges avec son PorteFeuille
	Person(string name_, unsigned int age_, long int kingdom_id, Money * money_);

	// On peut aussi faire des copies de Person
	Person(Person const & p);

	// GETTERS **********************************************

	// On peut recuperer le PorteFeuille d'une Person pour y faire des operations
	// (echange de valeur avec d'autres Person) 
	PorteFeuille getPorteFeuille() const;

	// On peut connaitre l'age d'une Person
	int getAge() const;

	// On peut bien-sur connaitre le nom d'une Person
	string getName() const;

	// Et aussi l'identifiant de son royaume
	long int getKingdom() const;

	// SETTERS *********************************************************

	// On peut changer le PorteFeuille d'une Person
	void setPorteFeuille(PorteFeuille const & porte_feuille_);

	// On peut changer son royaume
	void setKingdom(long int kingdom_id_);

	// On peut bien-sur changer son nom
	void setName(string & name_);

	// Et aussi son age (Pour le faire viellir ou rajeunir)
	void setAge(unsigned int const & age_);

	// AUTRES METHODES ********************************************

	// On peut avoir des informations sur une Person 
	string getString() const;

	// Une personne peut ne pas avoir de royaume
	// Dans ce cas, son royaume est NO_KINGDOM 
	static const long int NO_KINGDOM = 0;

	// On peut ecrire une Person dans un flux
	ostream & operator<<(ostream & stream);
	
protected:

	// Comme dit plus haut,
	// Une personne a un nom,
	string name;

	// Un age
	unsigned int age;

	// Un PorteFeuille pour ses transactions avec d'autres Person
	PorteFeuille porte_feuille;

	// Et un identifiant pour son royaume
	long int kingdom_id;
	
};

#endif