#include "Money.h"

Money::Money(string name_) : name(name_), valeur_marcher(1) {}
Money::Money(string name_, double valeur_marcher_) {
	if(valeur_marcher_ < 0)
		this->valeur_marcher = 0;
	else
		this->valeur_marcher = valeur_marcher_;
	this->name = name_;
}

string Money::getName() const {
	return this->name;
}
double Money::getValeur() const {
	return this->valeur_marcher;
}

void Money::setName(string const & name_) {
	this->name = name_;
}
void Money::setValeur(double const & valeur_marcher_) {
	if(valeur_marcher_ < 0)
		this->valeur_marcher = 0;
	else
		this->valeur_marcher = valeur_marcher_;
}

double Money::convertir(Money from_, Money to_, double quantite_) {
	return (to_.valeur_marcher * quantite_) / from_.valeur_marcher;
}

bool Money::operator=(Money const & money_) {
	return this->valeur_marcher == money_.valeur_marcher;
}
bool Money::operator>(Money const & money_) {
	return this->valeur_marcher > money_.valeur_marcher;
}
bool Money::operator<(Money const & money_) {
	return this->valeur_marcher < money_.valeur_marcher;
}

string Money::getString() const {
	return "( Money = name = " + name + " valeur_marcher = " + to_string(valeur_marcher) + ")";
}

ostream & Money::operator<<(ostream & stream) {
	stream << getString();
	return stream;
}