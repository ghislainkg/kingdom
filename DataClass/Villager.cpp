#include "Villager.h"

Villager::Villager(string name_, int age_, long int kingdom_id, Money * money_, Work travail_) :
	Person(name_, age_, kingdom_id, money_)
	, travail(travail_) {

}

Work Villager::getTravail() const {
	return this->travail;
}
void Villager::setTravail(Work const & travail_) {
	this-> travail = travail_;
}

string Villager::getStringOfWork(Work travail_) {
	switch(travail_) {
		case AGRICULTEUR :
			return "AGRICULTEUR";
		case MARCHANT :
			return "MARCHANT";
		case CHARPENTIER :
			return "CHARPENTIER";
		case ENSEIGNANT :
			return "ENSEIGNANT";
		default :
			return "NOT A VILLAGER";
	}
}

string Villager::getString() const {
	return "( Villager : name = " + name + " age = " + to_string(age) + " porte_feuille = " + porte_feuille.getString() + " kingdom_id = " + to_string(kingdom_id) + " travail = " + Villager::getStringOfWork(this->travail) + " porte_feuille = " + porte_feuille.getString() + " )";
}

ostream & Villager::operator<<(ostream & stream) {
	stream << getString() ;
	return stream;
}