#ifndef _VILLAGER_
#define _VILLAGER_

#include <iostream>
#include <string>

#include "Person.h"

// Definition de ce qu'est un paysan alias Villager dans ce monde.

using namespace std;

// D'abord, les Villagers occupe certaine travail (alias Work). Un villager peut donc etre :
typedef enum Work {

	AGRICULTEUR = 0, // Un agriculteur,
	MARCHANT = 1, // Un marchant,
	CHARPENTIER = 2, // Un charpentier, 
	ENSEIGNANT = 3 // ou Un enseignant

} Work;

// Un villager est avant tout une personne (Alias Person)
class Villager : public Person{

public:

	// CONSTRUCTEURS ********************************************

	// On peut creer un Villager avec le constructeur de la classe
	Villager(string name_, int age_, long int kingdom_id, Money * money_, Work travail_);
	

	// GETTERS **************************************************

	// On peut connaitre le travail d'un Villager
	Work getTravail() const;

	// SETTERS **************************************************

	// On peut aussi changer son travail
	void setTravail(Work const & travail_);

	// AUTRES METHODES ******************************************

	// On peut avoir les informations sur un Villager
	string getString() const;

	// On peut ecrire un Villager dans un flux de sortie
	ostream & operator<<(ostream & stream);

	// On peut avoir le nom d'un travail (Chaine de caracteres)
	static string getStringOfWork(Work travail_);

private:

	// Comme dit plus haut, un villager a un travail
	Work travail;
};

#endif