#ifndef _ARISTOCRAT_
#define _ARISTOCRAT_

#include <iostream>

#include "Person.h"

// Definition de ce qu'est un Aristocrat dans ce monde

using namespace std;

// Un Aristocrat occupe une haute fonction.
// Entre autre, il peut etre
typedef enum Grade
{
	KING = 0, // Un roi
	COMPTE = 1, // un Compte
	REGENT = 2 // Ou un Regent
} Grade;

// Un aristocrat est avant tout une Person
class Aristocrat : public Person {

public:

	// CONSTRUCTEURS **********************************************

	// On peut creer un Aristocrat avec un nom (name_),
	// un age (age_)
	// un identifiant de Kingdom (Celui du Kingdom auquel il appartient)
	// un Money (Sera celle de son PorteFeuille)
	// Et un Grade (grade_) (La fonction qu'il occupe)
	Aristocrat(string name_, int age_, long int kingdom_id, Money * money_, Grade grade_);

	// GETTERS ******************************************************

	// On peut connaitre le grade d'un Aristocrat
	Grade getGrade() const;

	// SETTERS ***************************************************

	// on peut changer le Grade d'un Aristocrat
	void setGrade(Grade grade_);

	// AUTRES METHODES ************************************************

	// On peut avoir des informations sur notre Aristocrat
	string getString() const;

	// On peut ecrire notre Aristocrat dans un flux de sortie
	ostream & operator<<(ostream & stream);

	// On peut Avoir la chaine de characteres decrivant un Grade
	static string getStringOfGrade(Grade grade_);
private:

	// Un Aristocrat Possede un Grade
	Grade grade;
};

#endif