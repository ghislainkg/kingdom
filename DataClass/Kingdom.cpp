#include "Kingdom.h"

Kingdom::Kingdom(string name_, Money * money_, int id_) {
	this->name = name_;
	this->king = NULL;

	this->nbr_persons = 0;
	population = vector<Person *>();
	this->id = id_;

	this->money = money_;
}
Kingdom::Kingdom(string name_, Money * money, int id_, Aristocrat * king_) {
	Kingdom(name_, money, id_);
	this->king = king_;
}

string Kingdom::getName() const {
	return this->name;
}
Aristocrat * Kingdom::getKing() const {
	return this->king;
}
Money * Kingdom::getMoney() const {
	return this->money;
}
vector<Person *> Kingdom::getPopulation() const {
	return this->population;
}
long int Kingdom::getId() {
	return this->id;
}
int Kingdom::getNbrPersons() const {
	return this->nbr_persons;
}

void Kingdom::setName(string const & name_) {
	this->name = name_;
}
void Kingdom::setKing(Aristocrat * king_) {
	if(king_->getGrade() == Grade::KING) {
		this->king = king_;
	}
}
void Kingdom::setMoney(Money * money_) {
	this->money = money_;
	for (vector<Person*>::iterator it = population.begin(); it != population.end() ; ++it) {
		Person * p = (*it);
		p->setPorteFeuille(
			p->getPorteFeuille().convertir(this->money)
		);
	}
}

void Kingdom::removePerson(Person * person_) {
	for(vector<Person *>::iterator it = this->population.begin();
		it != this->population.end();
		++it) {
		if(*it == person_) {
			this->population.erase(it);
			break;
		}
	}
	person_->setKingdom(Person::NO_KINGDOM);
	this->nbr_persons --;
}
void Kingdom::addPerson(Person * person_) {
	this->population.push_back(person_);
	this->nbr_persons ++;
	person_->setPorteFeuille( person_->getPorteFeuille().convertir(this->money) );
	person_->setKingdom(this->id);
}

string Kingdom::getString() const {
	return 	"(Kingdom : id = " + to_string(id) + " name = " + name + " nbr_persons = " + to_string(this->nbr_persons) + " king = " + king->getString() + " money = " + money->getString() + ")";
}

ostream & Kingdom::operator<<(ostream & stream) {
	stream << getString();
	return stream;
}