#include "PorteFeuille.h"

PorteFeuille::PorteFeuille() : valeur(0) {}
PorteFeuille::PorteFeuille(Money * money_) : money(money_), valeur(0) {}
PorteFeuille::PorteFeuille(Money * money_, double const & valeur_) : money(money_), valeur(valeur_) {}
PorteFeuille::PorteFeuille(PorteFeuille const & p) {
	this->money = p.money;
	this->valeur = p.valeur;
}

Money * PorteFeuille::getMoney() const {
	return this->money;
}
double PorteFeuille::getValeur() const {
	return this->valeur;
}

void PorteFeuille::setMoney(Money * money_) {
	this->money = money_;
}

void PorteFeuille::getValeurFrom(PorteFeuille & porteFeuille_, double quantite_) {
	porteFeuille_.valeur -= quantite_;
	double gain = Money::convertir(*(porteFeuille_.money), *(this->money), quantite_);
	this->valeur += gain;
}

PorteFeuille PorteFeuille::convertir(Money * money_) {
	PorteFeuille res(money_, Money::convertir(*(this->money), *money_, this->valeur));

	return res;
}


bool PorteFeuille::operator==(PorteFeuille const & porteFeuille_) {
	return (Money::convertir(*(porteFeuille_.money), *(this->money), porteFeuille_.valeur)) == this->valeur;
}
bool PorteFeuille::operator<(PorteFeuille const & porteFeuille_) {
	return (Money::convertir(*(porteFeuille_.money), *(this->money), porteFeuille_.valeur)) < this->valeur;
}
bool PorteFeuille::operator>(PorteFeuille const & porteFeuille_) {
	return (Money::convertir(*(porteFeuille_.money), *(this->money), porteFeuille_.valeur)) > this->valeur;
}

string PorteFeuille::getString() const {
	return "( PorteFeuille : money = " + money->getString() + " valeur = " + to_string(valeur) + " )";
}

ostream & PorteFeuille::operator<<(ostream & stream) {
	stream << getString();
	return stream;
}