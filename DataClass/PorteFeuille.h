#ifndef _PORTEFEUILLE_
#define _PORTEFEUILLE_

#include <iostream>
#include <string>

#include "Money.h"

// Definition de ce qu'est un porte feuille dans ce monde (alias PorteFeuille)

using namespace std;

class PorteFeuille {
public:

	// CONSTRUCTEURS *****************************************

	// On peut creer un porte feuille sans rien specifier, il est alors completement vide.
	// Ne pas le faire a moins de savoir ce qu'on fait
	// Dans ce cas, money = NULL et valeur = 0
	PorteFeuille();

	// On peut aussi en creer en specifiant la Devise (alias Money) qu'il contiendra
	// Dans ce cas, valeur = 0
	PorteFeuille(Money * money_);

	// Ou encore la Money et un contenu de depart (alias Valeur)
	PorteFeuille(Money * money_, double const & valeur_);

	// Ou encore en faisant la copie d'un autre PorteFeuille
	PorteFeuille(PorteFeuille const & p);

	// GETTERS ****************************************************

	// On peut connaitre la money d'un porte feuille
	Money * getMoney() const;

	// On peut aussi connaitre son contenu
	double getValeur() const;

	// SETTERS **********************************************************

	// Ou le changer
	void setMoney(Money * money_);

	// AUTRES METHODES *****************************************************

	// On peut recuperer une quantite de valeur (en Money du parametre porteFeuille)
	// que l'on converti en la money de notre porte feuille
	// le porteFeuille en parametre est alors deduit de cette valeur
	void getValeurFrom(PorteFeuille & porteFeuille, double quantite_);

	// On peut creer un Portefeuille contenant la valeur de celui ci mais convertie en money_ (le parametre)
	PorteFeuille convertir(Money * money_);

	// On peut avoir des informations sur notre PorteFeuille
	string getString() const;
	
	// On peut comparer les PorteFeuille
	bool operator==(PorteFeuille const & porteFeuille_);
	bool operator<(PorteFeuille const & porteFeuille_);
	bool operator>(PorteFeuille const & porteFeuille_);

	// On peut aussi les ecrires dans un flux
	ostream & operator<<(ostream & stream);

private:

	// Un PorteFeuille possede donc une Money unique
	Money * money;

	// Et un contenu (valeur) en sa Money
	double valeur;
};


#endif