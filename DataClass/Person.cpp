#include "Person.h"

Person::Person(string name_, unsigned int age_, long int kingdom_id, Money * money_) : 
	name(name_),
	age(age_),
	porte_feuille(PorteFeuille(money_)) {
}

Person::Person(Person const & p) {
	this->name = p.name;
	this->age = p.age;
	this->kingdom_id = p.kingdom_id;
	this->porte_feuille = p.porte_feuille;
}

PorteFeuille Person::getPorteFeuille() const {
	return this->porte_feuille;
}
int Person::getAge() const {
	return this->age;
}
string Person::getName() const {
	return this->name;
}
long int Person::getKingdom() const {
	return this->kingdom_id;
}

void Person::setPorteFeuille(PorteFeuille const & porte_feuille_) {
	this->porte_feuille = porte_feuille_;
}
void Person::setKingdom(long int kingdom_id_) {
	this->kingdom_id = kingdom_id_;
}
void Person::setName(string & name_) {
	this->name = name_;
}
void Person::setAge(unsigned int const & age_) {
	this->age = age_;
}

string Person::getString() const {
	return "(Person : name = " + name + " age = " + to_string(age) + " porte_feuille = " + porte_feuille.getString() + " kingdom_id = " + to_string(kingdom_id) + " )";
}

ostream & Person::operator<<(ostream & stream) {
	stream << getString();
	return stream;
}
