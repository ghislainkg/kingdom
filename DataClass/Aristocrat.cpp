#include "Aristocrat.h"

Aristocrat::Aristocrat(string name_, int age_, long int kingdom_id, Money * money_, Grade grade_) : 
	Person(name_, age_, kingdom_id, money_),
	grade(grade_) {
}

Grade Aristocrat::getGrade() const {
	return this->grade;
}

void Aristocrat::setGrade(Grade grade_) {
	this->grade = grade_;
}

string Aristocrat::getStringOfGrade(Grade grade_) {
	switch(grade_) {
		case KING :
			return "KING";
		case COMPTE :
			return "COMPTE";
		case REGENT :
			return "REGENT";
		default :
			return "NOT AN ARISTOCRAT";
	}
}

string Aristocrat::getString() const {
	return "(Aristocrat : name = " + this->name + " age = " + to_string(this->age) + " Grade = " + (Aristocrat::getStringOfGrade(getGrade())) + " porte_feuille = " + porte_feuille.getString() + ")";
}

ostream & Aristocrat::operator<<(ostream & stream) {
	stream << getString();
	return stream;
}